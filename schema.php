<?php

/**
 * Created by PhpStorm.
 * User: Nate
 * Date: 9/8/2017
 * Time: 1:28 PM
 */


require('includes/application_top.php');
require(DIR_WS_INCLUDES . 'template_top.php');


//__________This just renders the schema.php in the admin________________________//


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $sitename = tep_db_input($_POST['sitename']);
    $street = tep_db_input($_POST['street']);
    $city = tep_db_input($_POST['city']);
    $state = tep_db_input($_POST['state']);
    $zipcode = tep_db_input($_POST['zipcode']);
    $phone = tep_db_input($_POST['phone']);
    $fax = tep_db_input($_POST['fax']);
    $social = tep_db_input($_POST['social']);


    $sqlupdate = tep_db_query(
        "UPDATE `schema_vals`
        SET schema_value = 
        CASE 
        When schema_name = 'sitename' THEN '$sitename'
        When schema_name = 'street' THEN '$street' 
        When schema_name = 'city' THEN '$city'
        When schema_name = 'State' THEN '$state'
        When schema_name = 'zipcode' THEN '$zipcode'
        When schema_name = 'phone' THEN '$phone'
        When schema_name = 'fax' THEN '$fax'
        When schema_name = 'social' THEN '$social'
        Else schema_name
        end");

}

$valueRetrieve = tep_db_query("Select schema_name, schema_value FROM `schema_vals`");
$values = array();
while ($row = tep_db_fetch_array($valueRetrieve)) {
    $values[$row['schema_name']] = $row['schema_value'];
}


?>


<style>
    .example {
        margin-left: 20px;
    }

    form {
        float: left;
    }

    .results {
        float: right;
        border: 2px solid;
        width: 100%;
        max-width: 606px;

        font-weight: bold;
        line-height: 2em;
        margin-right: 500px;
        margin-top: 50px;

    }
</style>
<form action="schema.php" id="schema" method="post">
    Site Name:(Specify your own or let it prefill <?php echo '"' . STORE_NAME . '"' ?>)<br>
    <input type="text" name="sitename" value=<?php echo $values['sitename']; ?>><br>
    Street Address:(Required if Desired)<br>
    <input type="text" name="street" value=<?php echo $values['street']; ?>><br>
    City:(Required if Desired)<br>
    <input type="text" name="city" value=<?php echo $values['city']; ?>><br>
    State"2 letter abv:(Required if Desired)<br>
    <input type="text" name="state" value=<?php echo $values['state']; ?>><br>
    Zipcode:(Required if Desired)<br>
    <input type="text" name="zipcode" value=<?php echo $values['zipcode']; ?>><br>
    Phone:(Specify your own or let it prefill <?php echo '"' . STORE_TELEPHONE . '"' ?>)<br>
    <input type="text" name="phone" value=<?php echo $values['phone']; ?>><br>
    Fax:(Required if Desired)<br>
    <input type="text" name="fax" value=<?php echo $values['fax']; ?>><br>
    Social(Required if Desired)(must have a comma after each site except for last:<br>
    <textarea rows="8" cols="80" name="social">
<?php echo $values['social']; ?></textarea>
    <div class="example">
        example: <br>
        www.facebook.com,<br>
        www.linkedin.com,<br>
        www.youtube.com (no comma)<br>
    </div>

    <input type="submit">

</form>

<div class="results"><h1> Results</h1> <br>
    <h3> These are the reuslts that you should get after you initialize the schema admin console. There will be a
        error message for each other part that failed. Table schema_vars was created</h3>
    <p>Table schema_vars was populated <br> Table Admin_files was updated <br>SchemaOrg.php created succesfully<br>product
        attributes variable globals added<br>product attributes variable added<br>
    <h3> The Real Results</h3>
    </p> <?php echo schema::results(); ?> </div>

<?php require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>


<!-- Notes for the scripts
If you need multi pricing but the file was write protected so the script could not do what it needed to you will need to go to product_attributes in includes/modules and add the following

If you need to get multiple pricing points.  you will need to go to product_attributes in includes/modules. search for
// $languages_id, $cart, $currencies, $number_of_uploads;' and add $multiPricingArray as a global then look for
// ul class=\"products-options-listing\"> and set $multiPricingArray = $this->prodAttribs[0];';

You may also need to go to the shared template,  roll to the bottom and include schemaOrg.php from the included directory


This same rule will apply for schemaOrg.php.  Nothing will happen if that file did not get created.  scroll up to createSchemaOrg and
copy that code.  create the file "schemaOrg.php" in the includes directory.

or you can compare to base and find all of these.