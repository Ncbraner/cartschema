<?php
/**
 * Created by PhpStorm.
 * User: Nate
 * Date: 10/10/2017
 * Time: 11:00 AM
 */


/* Notes for the scripts




/*  This script will set up everything we need to do to make schema work in the cart easily.  It will add a schema link under SEO tools and allow manipulation of the schema data through the admin.
It will create a table to hold the values,  it will prefill what we need into that table, its going to change the adminfiles so it will show up under seotools,  its going to TRY to add some lines
to product attributes so that "offers" works for the products that have more than one pricing point.  If by chance that file was protected, then the report will tell you on the front end.  Read the comments at the bottom on what
you need to manually put in.*/

require('includes/application_top.php');
require(DIR_WS_INCLUDES . 'template_top.php');

//First lets take time to setup the ability to control Schema from the admin


// Were going to see if we have set this up before.  That way if this file gets called by accident we dont overwite our values.
if (tep_db_table_exists(schema_vals)) {
    $init = tep_db_query(" SELECT schema_value FROM schema_vals where schema_name = 'initalized'");
    $init = tep_db_fetch_array($init);
    $init = $init[schema_value];
}

//If for some reason you want to roll through setup again remove the comment on the next line
//$init = 1;

//If it has not been set up, lets make sure one last time that we want to and let it all get setup


if ($init != 1) {

    schema::createSchemaTable();
    schema::fillSchemaTable();
    schema::updateAdminFiles();
    schema::createSchemaOrg();
    schema::addLines();


}


//Below here is just the functions to put this all into the admin


//Below is all the functionality that should automatically initialize Schema in the admin under SEO tools
class schema
{
    public static $result;

//sets up schemaTable
    public static function createSchemaTable()
    {
        $createschema = tep_db_query("

            CREATE TABLE schema_vals (
            schema_id INT,
            schema_name VARCHAR(255) ,
            schema_value VARCHAR(255) 
      
            );"
        );
        if ($createschema) {
            schema::$result = " Table schema_vars was created<br>";
        } else {
            schema::$result = "Table schema_vars failed to create <br>";
        }
    }

    public static function fillSchemaTable()
    { // prefills table with values needed
        $fillSchTable = tep_db_query("
        INSERT INTO 
        schema_vals(schema_id, schema_name,schema_value)
        VALUES 
        (1,'sitename',''),
        (2,'street',''),
        (3,'city',''),
        (4,'state',''),
        (5,'zip',''),
        (6,'phone',''),
        (7,'fax',''),
        (8,'social',''),
        (9,'initalized',1)
        
        ");

        if ($fillSchTable) {
            schema::$result .= " Table schema_vars was populated <br>";
        } else {
            schema::$result .= "Table schema_vars failed to populate <br>";
        }
    }

    public static function updateAdminFiles()
    {
        //We are going to get the boxes id that belongs to Seo Tools
        $boxQuery = tep_db_query(" SELECT boxes_id FROM admin_files where files_title = 'Page Control'");
        $boxes_id = tep_db_fetch_array($boxQuery);
        $boxes_id = $boxes_id['boxes_id'];


        //Now we are going to get the SEO groups id

        $seoQuery = tep_db_query(" SELECT admin_groups_id FROM admin_groups where admin_groups_name LIKE \"%Seo%\"");
        $seo_id = tep_db_fetch_array($seoQuery);
        $seo_id = $seo_id['admin_groups_id'];


        //now we are going to update admin files with the datarow for the new schema.php


        $adminQuery = tep_db_query("
        INSERT INTO 
        admin_files(boxes_id, files_type, files_title, files_name, files_file, groups_ids, files_enabled, files_display)
        VALUES 
                   ($boxes_id,'primary','Schema','schema','schema.php','$seo_id','1','1');
        
        ");

        if ($adminQuery) {
            schema::$result .= " Table Admin_files was updated <br>";
        } else {
            schema::$result .= "Table admin_files failed to update <br>";
        }
    }


    public static function createSchemaOrg()
    {
        //We are going to create the schemaorg.php file in the includes dir
        //code to be put into schemaOrg.php

        $code = '<?php


        //Get values from database.  These will be set in the Schema link in the admin under SEO tools
        
        
        $valueRetrieve = tep_db_query("Select schema_name, schema_value FROM `schema_vals`");
        $values = array();
        while ($row = tep_db_fetch_array($valueRetrieve)) {
            $values[$row[\'schema_name\']] = $row[\'schema_value\'];
        }
        
        
        //Set these variables from the information you should have been provided
        $street = $values[\'street\'];
        $city = $values[\'city\'];
        $state = $values[\'state\'];
        $zipCode = $values[\'zipcode\'];
        $phone = $values[\'phone\'] ? $values[\'phone\'] : STORE_TELEPHONE;
        $fax = $values[\'fax\'];
        $siteName = $values[\'sitename\'] ? $values[\'sitename\'] : STORE_NAME;
        
        $social = $values[\'social\'];
        
        
        // Figure out what kind of schema to use for pricing ** price or offers **
        // If you need to get multiple pricing points.  you will need to go to product_attributes in includes/modules. search for 
        // $languages_id, $cart, $currencies, $number_of_uploads;\' and add $multiPricingArray as a global then look for 
        // ul class=\"products-options-listing\"> and set $multiPricingArray = $this->prodAttribs[0];\';
        
        
        $ARC = count($multiPricingArray);
        $price = "";
        $COUNT = 1;
        
        if ($product_info[\'products_price\'] > 0) {
        
            $price = \'
            "offers": {
                "@type": "Offer",
                        "availability": "http://schema.org/InStock",
                        "price": "\' . $product_info[\'products_price\'] . \'",
                        "priceCurrency": "USD"
          }\';
        
        } elseif ($multiPricingArray > 0) {
        
            $price = \'"offers": [\';
        
            foreach ($multiPricingArray as $index => $item) {
        
        
                $price .= \'
                {
                    "@type" : "Offer",
                            "name" : "\' . $item[options_values_name] . \'",
                            "price" : "\' . $item[options_values_price] . \'",
                            "priceCurrency" : "USD"
            } \';
        
        
                if ($COUNT < $ARC) {
                    $price .= ",";
                    $COUNT = $COUNT + 1;
                } else {
                    $price .= "]";
                }
            }
        
        }
        
        
        
        //now the magic happens//
        
        if (isset ($product_info[\'products_description\'])) {
            echo \'
            <script type="application/ld+json">
        {
            "@context": "http://schema.org",
          "@type": "Product",
          "description": "\' . htmlentities($product_info[\'products_description\']) . \'",
          "name": "\' . $product_info[\'products_name\'] . \'",
          "image": "\' . $product_info[\'products_image\'] . \'",
          \' . $price . \'
          }
        
        </script>\';
        
        };
        
        
        echo \'
              <script type="application/ld+json">// <![CDATA[
                    {
                        "@context": "http://schema.org",
                        "@type":"WebSite",
                        "name":" \' . $siteName . \'",
                        "url":"\' . DOMAIN . \'"
                    }
                    // ]]></script>
                    
                    <script type="application/ld+json">// <![CDATA[
                    {
                        "@context": "http://schema.org",
                        "@type": "Organization",
                        "address":{
                            "@type": "PostalAddress",
                            "addressCountry": "   United States       ",
                            "addressLocality": " \' . $city . \'",
                            "addressRegion": "\' . $state . \'       ",
                            "name": "\' . $siteName . \' ",
                            "postalCode": "  \' . $zipCode . \' ",
                            "streetAddress": "  \' . $street . \' "
                        },
                        "telephone": "   \' . $phone . \'  ",
                        "faxNumber": "        \' . $fax . \'      "
                        }
                    // ]]>
                    // ]]></script>      \';
        
        $arrCount = count($breadcrumb->_trail);
        echo \'
                            
                            
                                <script type="application/ld+json">
                                     {
                                      "@context": "http://schema.org",
                                       "@type": "BreadcrumbList",
                                        "itemListElement":
                                          [\';
        
        foreach ($breadcrumb->_trail as $index => $item) {
        
            echo \'
                                  {
                                  "@type": "ListItem",
                                    "position": \' . $index . \',
                                   "item":
                                    {
                                     "@id": "\' . htmlentities($item[link]) . \'",
                                    "name": "\' . htmlentities($item[title]) . \'"
                                        }
                                      }\';
            if ($arrCount == ($index + 1)) {
                echo "]} </script>";
        
            } else {
                echo \',\';
            };
        }
        
        if ($social != "") {
        
            echo \'
            <script type = "application/ld+json" >
        {
            "@context": "http://schema.org",
            "@type":"Organization",
            "name":"\' . $siteName . \'",
            "url":"\' . DOMAIN . \'",
            "sameAs": \' . json_encode(explode(",", $social)) . \'
        
        }
        </script >\';
        }
        
        ?>';
        //Now we create the file in the includes directory
        $create = $_SERVER['DOCUMENT_ROOT'] . "/includes/schemaOrg.php";


        if (fopen($create, "w") !== false) {
            $open = fopen($create, "w");
            fwrite($create, "$code");
            fclose($create);
            schema::$result .= "SchemaOrg.php created succesfully<br>";
        } else {
            schema::$result .= "SchemaOrg.php not added, file write protected<br>";
        }


    }
    public static function addLines()
    {
        //adds lines to product attributes to make multi teared pricing work

        //set $multiPricingArray variable as global


        $file = $_SERVER['DOCUMENT_ROOT'] . "includes/modules/products_attributes.php";

        if (file_exists($file) && ($read = file_get_contents($file) !== false)) {
            $read = file_get_contents($file);

            $search = '$languages_id, $cart, $currencies, $number_of_uploads;';

            $replace = '$languages_id, $cart, $currencies, $number_of_uploads, $multiPricingArray;';

            $read2 = str_replace($search, $replace, $read, $count);


            if (fopen($file, "w") !== false) {
                $open = fopen($file, "w");
                fwrite($open, "$read2");
                fclose($open);
                schema::$result .= "product attributes variable globals added<br>";
            } else {
                schema::$result .= "product attributes variable globals not added, file write protected<br>";
            }


            // Sets the variable to a value
            if (file_exists($file) && (fopen($file, "w") !== false)) {
                $search = '$tmp_html = ($wrapIt ? "<ul class=\"products-options-listing\">" : \'\');';
                $replace = '$tmp_html = ($wrapIt ? "<ul class=\"products-options-listing\">" : \'\');
                            $multiPricingArray = $this->prodAttribs[0];';
                $read2 = str_replace($search, $replace, $read2, $count);

                $open = fopen($file, "w");
                fwrite($open, "$read2");

                fclose($open);
                schema::$result .= "product attributes variable added<br>";
            } else {
                schema::$result .= "product attributes variable not added, file write protected? <br>";
            }
        }

    }

    public static function results()
    {
        if (schema::$result != "") {
            return schema::$result;
        } else {
            echo " Schema has already been set up!";
        }


    }


}


//__________This just renders the schema.php in the admin________________________//


?>


<style>
    .example {
        margin-left: 20px;
    }

    form {
        float: left;
    }

    .results {
        float: right;
        border: 2px solid;
        width: 100%;
        max-width: 606px;

        font-weight: bold;
        line-height: 2em;
        margin-right: 500px;
        margin-top: 50px;

    }
</style>


<div class="results"><h1> Results</h1> <br>
    <h3> These are the reuslts that you should get after you initialize the schema admin console. There will be a
        error message for each other part that failed. Table schema_vars was created</h3>
    <p>Table schema_vars was populated <br> Table Admin_files was updated <br>SchemaOrg.php created succesfully<br>product
        attributes variable globals added<br>product attributes variable added<br>
    <h3> The Real Results</h3>
    </p> <?php echo schema::results(); ?> </div>

<?php require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>

