<?php echo DOCTYPE; ?>
<html <?php echo HTML_PARAMS; ?>>
<head>
    <?php


    /*** Begin Header Tags SEO ***/
    if (file_exists(DIR_WS_INCLUDES . 'header_tags.php')) {
        require(DIR_WS_INCLUDES . 'header_tags.php');
    } else {
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>"/>
        <title><?php echo tep_output_string_protected($oscTemplate->getTitle()); ?></title>
        <?php
    }
    /*** End Header Tags SEO ***/
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?= BASE_HREF ?>"/>
    <?php
    $css = array();
    if (CART_RESPONSIVE != 'true') { ?>
        <style>
            .container {
                width: 1280px !important; /* Set this to the lg container size for your cart */
            }
        </style>
    <?php } ?>

    <!-- Links -->
    <!-- Bootstrap - include this even if it's not a responsive cart -->
    <?php
    $css = array(
        'ext/bootstrap/css/bootstrap.min.css',
        'ext/jquery/ui-1.11.4/jquery-ui.css',
//		'ext/datepicker/css/datepicker.css',
        'ext/jquery/colorbox/theme_1/colorbox.css',
        'includes/css/stylesheet.css',
        'includes/css/menu.css',
        'includes/css/headertags_seo_styles.css',
        'includes/css/slideshows.css',
        'includes/css/fancyfiles.css',
        'ext/bxslider-4/dist/jquery.bxslider.min.css',
        'ext/lightbox2/dist/css/lightbox.css',
        'includes/css/image_viewer.css',
        'includes/css/search.css'
    );
    if (CART_RESPONSIVE == 'true') {
        $css = array_merge($css, array(
            'includes/css/responsive_menu.css',
            'includes/css/responsive_faceted_search.css',
            'ext/mmenu/dist/css/jquery.mmenu.all.css',
            'ext/mmenu/dist/extensions/positioning/jquery.mmenu.positioning.css',
            'ext/mmenu/dist/extensions/pagedim/jquery.mmenu.pagedim.css',
            'includes/css/responsive.css',
            'ext/mmenu/dist/extensions/multiline/jquery.mmenu.multiline.css'
        ));
    } else {
        array_push($css, "includes/css/non-responsive.css");
    }
    if (DEBUG_LVL > 0) {
        echo "\n";
        foreach($css as $file) {
            echo '<link type="text/css" href="'.$file.'" rel="stylesheet"/>'."\n";
        }
    } else {
        echo '<link type="text/css" href="min/f='.implode(',', $css).'" rel="stylesheet"/>';
    }
    ?>
    <link rel="stylesheet" type="text/css" href="includes/css/print.css" media="print"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <?php
    $js = array(
        'includes/javascript/jquery-2.2.4.min.js',
        'includes/javascript/jquery-ui.min.js',
        'includes/javascript/jquery.browser.min.js',
        'ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js',
        'ext/jquery/colorbox/jquery.colorbox.js',
        'includes/javascript/jquery.blockUI.js',
        'includes/javascript/product_variables.js',
        'includes/javascript/jquery.hoverIntent.minified.js',
        'includes/javascript/general.js',
        'includes/javascript/savePDF.js',
        'includes/javascript/jquery.cycle.all.js',
        'includes/javascript/menu.js',
        'includes/javascript/search.js',
        'ext/bxslider-4/dist/jquery.bxslider.min.js'
    );
    if (CART_RESPONSIVE == 'true') {
        $js = array_merge($js, array(
            "ext/mmenu/dist/js/jquery.mmenu.all.min.js",
            "includes/javascript/responsive_menu.js"
        ));
    }
    if(PRODUCT_LISTING_FLOAT_HEAD == 'true') {
        array_push($js, "includes/javascript/jquery.floatThead.js");
    }

    if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) { // ???
        array_push($js, 'ext/jquery/ui/i18n/jquery.ui.datepicker-' . JQUERY_DATEPICKER_I18N_CODE . '.js');
    }

    if(MODULE_BOXES_FACETED_SEARCH_STATUS == 'True') {
        $js = array_merge($js, array(
            "includes/javascript/js-inherit.js",
            "includes/javascript/js-listbox.js",
//			"includes/javascript/jquery.nicescroll.min.js"
        ));
    }
    if (DEBUG_LVL > 0) {
        echo "\n";
        foreach($js as $file) {
            echo '<script type="text/javascript" src="'.$file.'"></script>'."\n";
        }
    } else {
        echo '<script type="text/javascript" src="min/f='.implode(',', $js).'"></script>';
    }
    ?>
    <?php if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) { // ??? ?>
        <script type="text/javascript">
            $.datepicker.setDefaults($.datepicker.regional['<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>']);
        </script>
    <?php } ?>

    <script type="text/javascript">
        var floatHead = false;
        <?php
        if(PRODUCT_LISTING_FLOAT_HEAD == 'true') {
        ?>
        floatHead = true;
        <?php } ?>
        jQuery(document).ready(function () {
//			$("#responsive_nav").css('display', 'block');
//			$("#responsive_faceted_search").css('display', 'block');
            <?php if(MOBILE_MENU_LOCAL_LEFT == 'true') { ?>
//			$('div.mm-panels li.selected > a').click();
            <?php } ?>
        });
    </script>
    <?php
    echo $oscTemplate->getBlocks('header_tags');

    // Determine bodyContent's number of columns:
    $bodyContentCols = 12;
    $pageHasLeftColumn = $oscTemplate->hasBlocks('boxes_column_left');
    $pageHasRightColumn = $oscTemplate->hasBlocks('boxes_column_right');
    if ($pageHasLeftColumn && !$hide_LeftColumn) {
        $bodyContentCols = $bodyContentCols - 3;
    }
    if ($pageHasRightColumn && !$hide_RightColumn) {
        $bodyContentCols = $bodyContentCols - 3;
    }
    ?>

    ([region:head])

    <?php //echo $oscTemplate->outputCSS(); // No longer need this because column widths for each page are determined elsewhere
    ?>
    <?php if (GOOGLE_ANALYTICS_STATUS == 'true' && GOOGLE_ANALYTICS_METHOD == 'Google Analytics') { ?>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', "<?php echo(GOOGLE_ANALYTICS_CODE); ?>", 'auto');
            ga('send', 'pageview');

        </script>
    <?php } ?>
    <script type="text/javascript">
        $(function(){
            listingAboveContent = <?php echo PRODUCT_LISTING_ABOVE_CONTENT == 'true' ? "true" : "false"; ?>;
            customTabsEnabled = <?php echo MODULE_CUSTOM_TABS_ENABLED == 'true' ? "true" : "false"; ?>;
            $('img').each(function(){
                if($(this).prop('alt') == '' && typeof $(this).data('image-name') != 'undefined') {
                    $(this).prop('alt', $(this).data('image-name'));
                }
                if($(this).prop('title') == '' && typeof $(this).data('image-name') != 'undefined') {
                    $(this).prop('title', $(this).data('image-name'));
                }
            });
        });
        var autocomplete = '<?= SEARCH_SPHINX_AUTOCOMPLETE ?>';
    </script>

</head>
<body <?= isset($tep_page_id) ? "id=\"page_{$tep_page_id}\"" : ""; ?>>
<?php if (GOOGLE_ANALYTICS_STATUS == 'true' && GOOGLE_ANALYTICS_METHOD == 'Tag Manager') { ?>
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=<?php echo GOOGLE_TAG_MANAGER_CODE; ?>"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '<?php echo GOOGLE_TAG_MANAGER_CODE; ?>//');</script>
    <!-- End Google Tag Manager -->
<?php } ?>
<div id="bodyWrapper" class="body-wrapper">
    <div id="headerWrapper" class="header-wrapper">
        <?php require(DIR_WS_INCLUDES . 'header.php'); ?>
    </div>

    ([region:sub_header])

    <div id="mainContentWrapper">
        <div id="mainContent" class="container"> <!-- Nothing inside this div can stretch full width -->
            <div class="row">
                <?php if (CART_RESPONSIVE == 'true') { ?>
                <div id="bodyContent" class="body-content col-md-<?php echo $bodyContentCols; ?> <?php echo $pageHasLeftColumn && !$hide_LeftColumn ? 'col-md-push-3' : '' ?>">
                    <?php } else { ?>
                    <div id="bodyContent" class="body-content col-xs-<?php echo $bodyContentCols; ?> <?php echo $pageHasLeftColumn && !$hide_LeftColumn ? 'col-xs-push-3' : '' ?>">
                        <?php } ?>
                        <?php if (!isset($tmpl_hide_breadcrumbs) || $tmpl_hide_breadcrumbs != TRUE) { ?>
                            <div class="breadcrumbs noprint"><?= $breadcrumb->trail(' &raquo; '); ?></div>
                        <?php } ?>
                        <?php if (!$hide_print) { ?>
                            <!--							<div id="printNav" class="noprint">-->
                            <!--								<a href="javascript:saveMPDF();" rel="nofollow"><img src="images/site/pdficon_small.gif" width="16" alt="Download PDF of this page" title="Download PDF of this page"></a>&nbsp;-->
                            <!--								<a href="javascript:window.print();"><img src="images/site/printer_small.gif" width="16" alt="Print this page" title="Print this page"></a><br>-->
                            <!--							</div>-->
                        <?php } ?>
                        ([region:body])
                    </div>
                    ([region:left_column])
                    ([region:right_column])
                    <div id="productboxes" class="col-xs-12"></div>
                </div>
            </div> <!-- end mainContent -->
            <?php // Custom homepage content outside of mainContent so that it can be full width
            if (isset($tep_page_id) && $tep_page_id == 'homepage') { ?>

            <?php } ?>
        </div> <!-- end mainContentWrapper -->
        <?php
        if (CART_RESPONSIVE == 'true') {
            $m = new menu_responsive();
            echo $m->buildMenu();
            echo '<div id="responsive_faceted_search_container"></div>';
        } ?>

        <div class="footerWrapper">
            <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
            <?php require(DIR_WS_INCLUDES . 'schemaOrg.php'); ?>


        </div>
        <div id="nicescroll"></div>
    </div> <!-- end bodyWrapper -->

    <?php echo $oscTemplate->getBlocks('footer_scripts'); ?>
    <!-- //EOF: Google Analytics MD Contib//-->

    <?php // Lightbox. This needs to be included after the DOM has loaded for the image galleries to work properly ?>
    <script src="ext/lightbox2/dist/js/lightbox.min.js"></script>
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true
        })
    </script>
    <?php // End Lightbox ?>
</body>
</html>