<?php


//Get values from database.  These will be set in the Schema link in the admin under SEO tools

if (tep_db_table_exists('schema_vals')) {
    $valueRetrieve = tep_db_query("Select schema_name, schema_value FROM `schema_vals`");
    $values = array();
    while ($row = tep_db_fetch_array($valueRetrieve)) {
        $values[$row['schema_name']] = $row['schema_value'];
    }
}



//Set these variables from the information you should have been provided
$street = $values['street'];
$city = $values['city'];
$state = $values['state'];
$zipCode = $values['zipcode'];
$phone = $values['phone'] ? $values['phone'] : STORE_TELEPHONE;
$fax = $values['fax'];
$siteName = $values['sitename'] ? $values['sitename'] : STORE_NAME;

$social = $values['social'];


// Figure out what kind of schema to use for pricing ** price or offers **

$ARC = count($multiPricingArray);
$price = "";
$COUNT = 1;

if ($product_info['products_price'] > 0) {

    $price = '
    "offers": {
        "@type": "Offer",
                "availability": "http://schema.org/InStock",
                "price": "' . $product_info['products_price'] . '",
                "priceCurrency": "USD"
  }';

} elseif ($multiPricingArray > 0) {

    $price = '"offers": [';

    foreach ($multiPricingArray as $index => $item) {


        $price .= '
        {
            "@type" : "Offer",
                    "name" : "' . $item[options_values_name] . '",
                    "price" : "' . $item[options_values_price] . '",
                    "priceCurrency" : "USD"
    } ';


        if ($COUNT < $ARC) {
            $price .= ",";
            $COUNT = $COUNT + 1;
        } else {
            $price .= "]";
        }
    }

}



//now the magic happens//

if (isset ($product_info['products_description'])) {
    echo '
    <script type="application/ld+json">
{
    "@context": "http://schema.org",
  "@type": "Product",
  "description": "' . htmlentities($product_info['products_description']) . '",
  "name": "' . $product_info['products_name'] . '",
  "image": "' . $product_info['products_image'] . '",
  ' . $price . '
  }

</script>';

};


echo '
      <script type="application/ld+json">// <![CDATA[
            {
                "@context": "http://schema.org",
                "@type":"WebSite",
                "name":" ' . $siteName . '",
                "url":"' . DOMAIN . '"
            }
            // ]]></script>
            
            <script type="application/ld+json">// <![CDATA[
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "address":{
                    "@type": "PostalAddress",
                    "addressCountry": "   United States       ",
                    "addressLocality": " ' . $city . '",
                    "addressRegion": "' . $state . '       ",
                    "name": "' . $siteName . ' ",
                    "postalCode": "  ' . $zipCode . ' ",
                    "streetAddress": "  ' . $street . ' "
                },
                "telephone": "   ' . $phone . '  ",
                "faxNumber": "        ' . $fax . '      "
                }
            // ]]>
            // ]]></script>      ';

$arrCount = count($breadcrumb->_trail);
echo '
                    
                    
                        <script type="application/ld+json">
                             {
                              "@context": "http://schema.org",
                               "@type": "BreadcrumbList",
                                "itemListElement":
                                  [';

foreach ($breadcrumb->_trail as $index => $item) {

    echo '
                          {
                          "@type": "ListItem",
                            "position": ' . $index . ',
                           "item":
                            {
                             "@id": "' . htmlentities($item[link]) . '",
                            "name": "' . htmlentities($item[title]) . '"
                                }
                              }';
    if ($arrCount == ($index + 1)) {
        echo "]} </script>";

    } else {
        echo ',';
    };
}

if ($social != "") {

    echo '
    <script type = "application/ld+json" >
{
    "@context": "http://schema.org",
	"@type":"Organization",
	"name":"' . $siteName . '",
	"url":"' . DOMAIN . '",
	"sameAs": ' . json_encode(explode(",", $social)) . '

}
</script >';
}



?>