<?php
/*
 $Id$

ECW products attributes output class


*/

class products_attributes {

	
	public $listingMode = 'form';
	public $headingCells = array();
	public $listingCells = array();
	public $outputType = 'ul';
	
	public $maxFileSize;
	
	public $products_tax_class_id = 0;
	
	public $attribClass = '';
	public $image_default_ext = 'jpg';
	
	public $attrsInclude = '';
	public $attrsExclude = '';
	public $fieldsOnly = false;
	public $prodInfo = array();
	public $valsListWrap = true;
	public $outputFldExtras = true;
	
	public $optionsCommon = '';
	public $optionTypes = array();
	public $prodOptions = array();
	public $prodAttribs = array();
	public $pAttribsCt = 0;
	
	protected $prodID = 0;
	protected $textOptVal_id = OPTIONS_VALUE_TEXT_ID;
	
	protected $textTypes = array();
	protected $textTypes_dims = array();
	
	
	public function __construct($pid = 0) {
		global $product_info;
		
		$this->prodID = $pid;
		
		// , OPTIONS_TYPE_FILE
		$this->textTypes = array(OPTIONS_TYPE_TEXT, OPTIONS_TYPE_TEXTAREA, OPTIONS_TYPE_QUANTITY, OPTIONS_TYPE_DIM);
		$this->textTypes_dims = array(OPTIONS_TYPE_QUANTITY, OPTIONS_TYPE_DIM);
		
		$this->maxFileSize = ini_get('upload_max_filesize');
		
		if((isset($product_info) && is_array($product_info) && count($product_info) > 0)) {
			$this->prodInfo = $product_info;
		   	if(isset($product_info['products_tax_class_id']) && $product_info['products_tax_class_id'] > 0) {
				$this->products_tax_class_id = $product_info['products_tax_class_id'];
			}
		}
		
	}
	
	protected function hasAttribs() {
		global $customer_group_id, $languages_id;
	
		$opts_sql = "select count(*) as total from " . TABLE_PRODUCTS_ATTRIBUTES . " pa 
				     inner join " . TABLE_PRODUCTS_OPTIONS . " popt on pa.options_id = popt.products_options_id 
				       and popt.language_id = '" . (int)$languages_id . "' 
					 where pa.products_id='{$this->prodID}'";
		
		if(ENABLE_CUSTOMER_GROUPS > 0) {
			// $opts_sql .= " and find_in_set('{$customer_group_id}', pa.attributes_hide_from_groups) = 0";
		}
		
		if(tep_not_null($this->attrsInclude) || tep_not_null($this->attrsExclude)) {
			$_fOp = (tep_not_null($this->attrsExclude) ? "not " : '');
			$_fVals = (tep_not_null($this->attrsExclude) ? $this->attrsExclude : $this->attrsInclude);
			$opts_sql .= " and popt.products_options_type {$_fOp}in({$_fVals})";
		}
		
		// dump($opts_sql);
		
		$opts_qry = tep_db_query($opts_sql);
		$opts = tep_db_fetch_array($opts_qry);
		return $opts['total'] > 0;
	}
	
	protected function set_option_types() {
		global $languages_id;
		$ot_qry = tep_db_query("select * from " . TABLE_PRODUCTS_OPTIONS_TYPES . " where language_id = '{$languages_id}'");
		while($ot = tep_db_fetch_array($ot_qry)) {
			$this->optionTypes[$ot['products_options_types_id']] = array('key' => $ot['options_types_key'], 
																		 'name' => $ot['products_options_types_name']);
		}
	}
	
	protected function get_group_attribute_pricing($gid, $aid) {
		if(ENABLE_CUSTOMER_GROUPS > 1) {
			$qry = tep_db_query ("select * from " . TABLE_PRODUCTS_ATTRIBUTES_GROUPS . " 
							  	  where customers_group_id =  " . (int)$gid . " and 
							  	  products_attributes_id =  " . (int)$aid);
			if($qry && tep_db_num_rows($qry) > 0) {
				return tep_db_fetch_array($qry);
			}
		}
		return false;
	}
	
	public function set_prodID($pid) {
		$this->prodID = (int)$pid;
	}
	
	public function get_prodID() {
		return $this->prodID;
	}
	
	public function set_varVal($var, $val) {
		$this->{$var} = $val;
	}
	
	public function get_varVal($var) {
		return $this->{$var};
	}
	
	public function set_textTypes($types) {
		if(is_array($types)) {
			$this->textTypes = $types;
		} elseif(is_string($types) && strpos($types, ',')) {
			$this->textTypes = preg_split("/,[\s]*/i", $types);
		}
	}
	
	public function get_textTypes() {
		return $this->textTypes;
	}
	
	public function set_attribClass($aclass) {
		$this->attribClass = $aclass;
		if($aclass == 'cut-stock') {
			$this->attrsInclude = implode(",", array(OPTIONS_TYPE_DIM, OPTIONS_TYPE_QUANTITY));
		}
	}
	
	public function set_prod_attribs($oid = 0) {
		global $customer_group_id, $languages_id;
		
		$pattr = array();
		
		$pa_select = "select distinct pa.products_attributes_id, pa.options_id, pa.options_values_id, pa.options_values_price, pa.price_prefix, pa.attribute_sort, 
					  popt.products_options_name, popt.products_options_type, popt.products_options_length, popt.products_options_comment, 
					  pov.products_options_values_name as options_values_name";
		
		$pa_from = "from " . TABLE_PRODUCTS_ATTRIBUTES . " pa";
		$pa_from .= " inner join " . TABLE_PRODUCTS_OPTIONS . " popt on pa.options_id = popt.products_options_id 
					  and popt.language_id = '" . (int)$languages_id . "'";
		$pa_from .= " inner join " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov on pa.options_values_id = pov.products_options_values_id 
					 	and pov.language_id = '" . (int)$languages_id . "'";
		
		$pa_where = "where pa.products_id='{$this->prodID}'";
		if($oid > 0) {
			$pa_where .= " and pa.options_id = '{$oid}'";
		}
		if(ENABLE_CUSTOMER_GROUPS > 0) {
			if($customer_group_id > 1) {
				$pa_select .= ", pag.options_values_price as groups_values_price, pag.price_prefix as groups_price_prefix";
				$pa_from .= " left join " . TABLE_PRODUCTS_ATTRIBUTES_GROUPS . " pag on pag.products_attributes_id = pa.products_attributes_id 
							 	and pag.customers_group_id = {$customer_group_id}";
			}
			$pa_where .= " and find_in_set('{$customer_group_id}', pa.attributes_hide_from_groups) = 0";
		}
		
		$pa_order = "order by popt.attribute_sort, pa.attribute_sort";
		
		if(tep_not_null($this->attrsInclude) || tep_not_null($this->attrsExclude)) {
			$_fOp = (tep_not_null($this->attrsExclude) ? "not " : '');
			$_fVals = (tep_not_null($this->attrsExclude) ? $this->attrsExclude : $this->attrsInclude);
			$pa_where .= " and popt.products_options_type {$_fOp}in({$_fVals})";
		}
		
		$pa_sql = "{$pa_select} {$pa_from} {$pa_where} {$pa_order}";
		
		// dump($pa_sql);
		
		$pa_qry = tep_db_query($pa_sql);
		if($pa_qry && tep_db_num_rows($pa_qry) > 0) {
			while($pa = tep_db_fetch_array($pa_qry)) {
				$this->pAttribsCt++;
				$this->prodOptions[$pa['options_id']] = $pa['products_options_type'];
				$this->prodAttribs[$pa['options_id']][$pa['options_values_id']] = $pa;
				
			}
		}
	}
	
	
	public function build_form($reset = false) {
		global $customer_group_id, $languages_id, $cart, $currencies, $number_of_uploads, $multiPricingArray;
		
		if(!$this->hasAttribs()) {
			return '';
		}
		
		if($reset || $this->pAttribsCt == 0) {
			$this->set_prod_attribs();
		}
		
		if($this->prodAttribs && is_array($this->prodAttribs) && count($this->prodAttribs) > 0) {
			$textCounter_init = false;
			
			$flds_noCounter = $this->textTypes_dims;
			$flds_noCounter[] = OPTIONS_TYPE_FILE;
			
			// Loop through products options
			foreach($this->prodOptions as $optId => $opt_type) {
				if(!is_int($opt_type)) {
					$opt_type = (int)$opt_type;
				}
				
				$opt_ID = (int)$optId;
				$opt_name = '';
				$opt_length = '';
				$tmp_html = '';
				
				$_appendHeading = '';
				$_fldParams = '';
				
				switch($opt_type) {
					case in_array($opt_type, $this->textTypes):
						// OPTIONS_TYPE_TEXT(2), OPTIONS_TYPE_TEXTAREA(5), OPTIONS_TYPE_QTY(8), OPTIONS_TYPE_DIM(9)
						
						$vVals = $this->prodAttribs[$opt_ID][$this->textOptVal_id];
						
						$opt_name = $vVals['products_options_name'];
						$opt_length = $vVals['products_options_length'];
						
						$isTextarea = ($opt_type == OPTIONS_TYPE_TEXTAREA ? true : false);
						
						$_elemId = TEXT_PREFIX . $opt_ID;
						$tmp_html_fld_pbar = '';
						$fld_class = '';
						
						if(($opt_length > 0 && !in_array($opt_type, $this->textTypes_dims)) && 
						   (((!$isTextarea && OPTIONS_TYPE_PROGRESS == 'Text') || ($isTextarea && OPTIONS_TYPE_PROGRESS == 'TextArea')) || OPTIONS_TYPE_PROGRESS == 'Both') ) {
							$fld_class .= " counter";
							$textCounter_init = true;
							$tmp_html_fld_pbar = <<<OPTJS
									<div id="counterbar_{$opt_ID}" class="counter-bar"><div id="progressbar_{$opt_ID}" class="counter-progress"></div></div>
									<script type="text/javascript">
										textCounter(document.getElementById("id[{$_elemId}]"), "progressbar_{$opt_ID}", {$opt_length}, "counterbar_{$opt_ID}");	
									</script>
OPTJS;
						}
						
						$_params = 'id="id[' . $_elemId . ']" data-optlen="' . $opt_length . '"';
						if($opt_type == OPTIONS_TYPE_DIM) {
							$fld_class .= " pa-dims";
						} elseif($opt_type == OPTIONS_TYPE_QUANTITY) {
							$fld_class .= " pa-qty";
						}
						
						$_fldName = 'id[' . $this->prodID . '][' . $_elemId . ']';
						
						if($opt_type == OPTIONS_TYPE_TEXTAREA) {
							$_params .= " rows=\"5\" wrap=\"soft\" class=\"attribs-fld type-textarea{$fld_class}\"";
							$tmp_html = tep_draw_textarea_field($_fldName, "soft", 220, 5, $cart->contents[$this->prodID]['attributes_values'][$opt_ID], $_params);
						} else {
							$_params .= " maxlength=\"{$opt_length}\" class=\"attribs-fld type-text{$fld_class}\"";
							$tmp_html = tep_draw_input_field($_fldName, $cart->contents[$this->prodID]['attributes_values'][$opt_ID], $_params);
						}
						
						if($this->outputFldExtras) {
							$tmp_html_price = '';
							if(ENABLE_CUSTOMER_GROUPS > 0 && isset($vVals['groups_values_price']) && $vVals['groups_values_price'] != 0) {
								$tmp_html_price .= ' (' . $vVals['groups_price_prefix'] . $currencies->display_price($vVals['groups_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
							} elseif($vVals['options_values_price'] != '0') {
								$tmp_html_price .= ' (' . $vVals['price_prefix'] . $currencies->display_price($vVals['options_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
							}
							$tmp_html .= ($isTextarea ? "<br />" : "&nbsp;");
							$tmp_html .= (tep_not_null($vVals['products_options_comment']) ? " {$vVals['products_options_comment']}" : '');
							$tmp_html .= "{$tmp_html_price}{$tmp_html_fld_pbar}";
						}
							
						$_heading = $opt_name;
						$_listing = $tmp_html;
						
						break;
					case OPTIONS_TYPE_RADIO:
					case OPTIONS_TYPE_CHECKBOX:
						// OPTIONS_TYPE_RADIO(3), OPTIONS_TYPE_CHECKBOX(4)
						
						if(count($this->prodAttribs[$opt_ID]) > 0) {
							$optCt = count($this->prodAttribs[$opt_ID]);
							$wrapIt = ($this->valsListWrap && $optCt > 1 ? true : false);
							$valsLoop = 0;
							
							$tmp_html = ($wrapIt ? "<ul class=\"products-options-listing\">" : '');
                            $multiPricingArray = $this->prodAttribs[0];



							
							foreach($this->prodAttribs[$opt_ID] as $vId => $vVals) {

								$checked = false;
								$tmp_html .= ($wrapIt ? "<li>" : '');
								if($valsLoop == 0) {
									$opt_name = $vVals['products_options_name'];
									if(!(isset($cart->contents[$this->prodID]['attributes'][$opt_ID]) && tep_not_null($cart->contents[$this->prodID]['attributes'][$opt_ID]))) {
										$checked = true;
									}
								}
								if(isset($cart->contents[$this->prodID]['attributes'][$opt_ID]) && $cart->contents[$this->prodID]['attributes'][$opt_ID] == $vId) {
									$checked = true;
								}
								
								if($opt_type == OPTIONS_TYPE_RADIO) {
									if($optCt > 1) {
										$tmp_html .= tep_draw_radio_field('id[' . $this->prodID . '][' . $opt_ID . ']', $vId, $checked) . " ";
									} else {
										$tmp_html = tep_draw_hidden_field('id[' . $this->prodID . '][' . $opt_ID . ']', $vId);
									}
									$tmp_html .= $vVals['options_values_name'];
								} else {
									$tmp_html .= tep_draw_checkbox_field('id[' . $this->prodID . '][' . $opt_ID . ']', $vId);
									if($optCt > 1) {
										$tmp_html .= " {$vVals['options_values_name']}";
									}
								}
								
								if($this->outputFldExtras) {
									$tmp_html .= (tep_not_null($vVals['products_options_comment']) ? " {$vVals['products_options_comment']}" : '');
									if(ENABLE_CUSTOMER_GROUPS > 0 && $vVals['groups_values_price'] != 0 ) {
										$tmp_html .= ' (' . $vVals['groups_price_prefix'] . $currencies->display_price($vVals['groups_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
									} elseif ($vVals['options_values_price'] != '0') {
										$tmp_html .= ' (' . $vVals['price_prefix'] . $currencies->display_price($vVals['options_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
									}
								}
								$tmp_html .= ($wrapIt ? "</li>\n" : '');
								$valsLoop++;
							}
							$tmp_html .= ($wrapIt ? "</ul>\n" : '');
						}
						
						$_heading = $opt_name;
						$_listing = $tmp_html;
						
						break;
					case OPTIONS_TYPE_FILE:
						$number_of_uploads++;
						
						$vVals = $this->prodAttribs[$opt_ID][$this->textOptVal_id];
						$opt_name = $vVals['products_options_name'];
						
						if($this->outputFldExtras) {
							$tmp_html_price = '';
							if(ENABLE_CUSTOMER_GROUPS > 0 && isset($vVals['groups_values_price']) && $vVals['groups_values_price'] != 0) {
								$tmp_html_price .= ' (' . $vVals['groups_price_prefix'] . $currencies->display_price($vVals['groups_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
							} elseif($vVals['options_values_price'] != '0') {
								$tmp_html_price .= ' (' . $vVals['price_prefix'] . $currencies->display_price($vVals['options_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
							}
						}
						
						//BOF - Zappo - Option Types v2 - Added dropdown with previously uploaded files
						if ($old_uploads == true) {
							unset($uploaded_array);
						}
						$uploaded_array[] = array('id' => '', 'text' => TEXT_NONE);
						$uploaded_files_query = tep_db_query("select files_uploaded_name from " . TABLE_FILES_UPLOADED . " where sesskey = '" . tep_session_id() . "' or customers_id = '" . (int)$customer_id . "'");
						while ($uploaded_files = tep_db_fetch_array($uploaded_files_query)) {
							$uploaded_array[] = array('id' => $uploaded_files['files_uploaded_name'], 
													  'text' => $uploaded_files['files_uploaded_name'] . ($tmp_html_price ? ' - ' . $tmp_html_price : ''));
							$old_uploads = true;
						}
						
						$tmp_html = '<input type="file" name="id[' . $this->prodID . '][' . TEXT_PREFIX . $opt_ID . ']">'; // File field with new upload
						$tmp_html .= tep_draw_hidden_field(UPLOAD_PREFIX . $number_of_uploads, $opt_ID); // Hidden field with number of this upload (for this product)
						$tmp_html .= $tmp_html_price;
						if($old_uploads == true) {
							$tmp_html .= '<br>' . tep_draw_pull_down_menu(TEXT_PREFIX . UPLOAD_PREFIX . $number_of_uploads, $uploaded_array, $cart->contents[$this->prodID]['attributes_values'][$opt_ID]);
						}
						//EOF - Zappo - Option Types v2 - Added dropdown with previously uploaded files
						
						$_heading = $opt_name;
						$_listing = $tmp_html;
						
						break;
					default:
						// Default: OPTIONS_TYPE_SELECT(1), OPTIONS_TYPE_IMAGE(7)
						
						$attribs_array = array();
						// $attribs_array[] = array('id' => '', 'text' => PULL_DOWN_DEFAULT);
						
						$valsLoop = 0;
						if(count($this->prodAttribs[$opt_ID]) > 0) {
							$optCt = count($this->prodAttribs[$opt_ID]);
							$firstImg = '';
							
							foreach($this->prodAttribs[$opt_ID] as $vId => $vVals) {

								$opt_name = $vVals['products_options_name'];
								$val_name_text = $vVals['options_values_name'];
								
								if($opt_type == OPTIONS_TYPE_IMAGE) {
									$val_array = preg_split("/[\.\s]/", $vVals['options_values_name']);
									$val_name_text = ucwords(str_replace(array('-', '_'), " ", $val_array[0]));
									
									$_iName = (defined('OPTIONS_TYPE_IMAGEPREFIX') ? OPTIONS_TYPE_IMAGEPREFIX : '');
									$_iName .= (OPTIONS_TYPE_IMAGENAME == 'Name') ? trim($val_array[0]) : $vId;
									$_iName .= (OPTIONS_TYPE_IMAGELANG == 'Yes' ? '_' . $languages_id : '');
									$_iExt = (count($val_array) > 1 ? trim($val_array[1]) : $this->image_default_ext);
									
									$imgPath = OPTIONS_TYPE_IMAGEDIR . "{$_iName}.{$_iExt}";
									
									if(is_file($imgPath)) {
										$opt_images[$vId] = "<img src=\"{$imgPath}\" alt=\"{$opt_name}\" title=\"{$opt_name}\">";
										
										if(isset($cart->contents[$this->prodID]['attributes'][$opt_ID]) && ($cart->contents[$this->prodID]['attributes'][$opt_ID] == $vId)) {
											$firstImg = $opt_images[$vId];
										} elseif(!(tep_not_null($firstImg) && $valsLoop > 0)) {
											$firstImg = $opt_images[$vId];
										}
									}
								}
								
								$attribs_array[$valsLoop] = array('id' => $vId, 
																  'text' => $val_name_text);
								
								// BOF SPPC attributes mod
								if(ENABLE_CUSTOMER_GROUPS > 0 && $vVals['groups_values_price'] != 0 ) {
									$attribs_array[$valsLoop]['text'] .= ' (' . $vVals['groups_price_prefix'] . $currencies->display_price($vVals['groups_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
									// EOF SPPC attributes mod
								} elseif($vVals['options_values_price'] != '0') {
									$attribs_array[$valsLoop]['text'] .= ' (' . $vVals['price_prefix'] . $currencies->display_price($vVals['options_values_price'], tep_get_tax_rate($this->prodInfo['products_tax_class_id'])) .')';
								}
								$valsLoop++;
							}
							
							if($opt_type == OPTIONS_TYPE_IMAGE && count($opt_images)) {
								// BOF - Zappo - PreLoad the Images
								$tmp_html = '<div id="ImagePreload">';
								$tmp_html .= implode(" ", $opt_images);
								$tmp_html .= "</div>\n";
								// EOF - Zappo - PreLoad the Images
								
								$_appendHeading .= "\n" . '<script type="text/javascript">var ImageText_' ."{$this->prodID}_{$opt_ID}". ' = new Array(\'' . implode("', '", $opt_images) . '\')</script>';
								$tmp_html .= "<center><div class=\"main\" id=\"ImageSelect_{$this->prodID}_{$opt_ID}\">";
								$tmp_html .= "{$firstImg}</div></center>\n";
								$_fldParams .= 'onChange="document.getElementById(\'ImageSelect_' . "{$this->prodID}_{$opt_ID}" . '\').innerHTML=ImageText_' . "{$this->prodID}_{$opt_ID}" . '[this.selectedIndex];"';
							}
						}
						
						$selected_attribute = false;
						if(isset($cart->contents[$this->prodID]['attributes'][$opt_ID])) {
							$selected_attribute = $cart->contents[$this->prodID]['attributes'][$opt_ID];
						}
						
						$_heading = $opt_name;
						$_listing = tep_draw_hidden_field('id[' . $this->prodID . '][' . $opt_ID . ']', $attribs_array[0]['id']) . $attribs_array[0]['text'];
						if(count($attribs_array) > 1) {
							$_listing = tep_draw_pull_down_menu('id[' . $this->prodID . '][' . $opt_ID . ']', $attribs_array, $selected_attribute, $_fldParams);
						}
						if($this->outputFldExtras) {
							$_listing .= (tep_not_null($vVals['products_options_comment']) ? " {$vVals['products_options_comment']}\n" : '');
							$_listing .= $tmp_html;
						}
						
						
						break;
					
				}
				
				if($this->attribClass == 'cut-stock') {
					if($opt_type == OPTIONS_TYPE_DIM) {
						// $this->headingCells['dims'] = TABLE_HEADING_ATTRIBS_CUT_TO_SIZE_IN;
						$this->headingCells['dims'][] = $_heading;
						$this->listingCells['dims'][] = $_listing;
					} elseif($opt_type == OPTIONS_TYPE_QUANTITY) {
						// $this->headingCells['qty'] = TABLE_HEADING_ATTRIBS_CUT_QTY;
						$this->headingCells['qty'] = $_heading;
						$this->listingCells['qty'] = $_listing;
					}
				} else {
					$this->headingCells[$opt_ID] = "<strong>{$_heading}:</strong>{$_appendHeading}";
					$this->listingCells[$opt_ID] = $_listing;
				}
				
				if($textCounter_init && tep_not_null($opt_length)) {
					$this->optionsCommon = <<<OPTJS
							<script type="text/javascript">
								$(".type-text.counter, .type-textarea.counter").on("focus keydown keyup", function() {
									var oid = this.id.replace(/[^0-9]/ig, '');
									textCounter(this, 'progressbar_' + oid, this.dataset.optlen)
								});	
							</script>
OPTJS;
				}
				
			}
		}
		
	}
	
	
	public function draw_attribs() {
		
		if($this->pAttribsCt < 1) {
			return '';
		}
		
		$aOut = "";
		
		// Build ul list
		if($this->outputType == "ul") {
			$aOut .= "<ul class=\"pa-options-listing listing-ul\">";
			foreach($this->listingCells as $aKey => $aListing) {
				$aOut .= "<li>";
				$aLabel = $this->headingCells[$aKey];
				if($aKey == 'dims') {
					$aLabel = implode(" X ", $aLabel);
					$aListing = implode(" X ", $aListing);
				}
				$aOut .= "	<div class=\"pa-label\">{$aLabel}</div>\n";
				$aOut .= "	<div class=\"pa-listing\">{$aListing}</div>\n";
				$aOut .= "</li>";
			}
			$aOut .= "</ul>\n";
		}
		// End ul list
		
		$aOut .= $this->optionsCommon;
		
		return $aOut;
	}
	
	// Get attributes listing for cart display
	//BOF quick rfq checkout
	public function draw_cart_summary($prod, $return_array = false) {
		global $languages_id, $customer_id, $currencies;
	    $attribs_array = array();
	//EOF quick rfq checkout    
		$pId = tep_get_prid($prod['id']);
	
		$optsOut = '';
		if(is_array($prod['attributes']) && count($prod['attributes']) > 0) {
			
			if($this->pAttribsCt == 0) {
				$this->set_prod_attribs();
			}
			
			$optsOut = "<ul class=\"pa-options-list smallText\">";
			foreach($prod['attributes'] as $option => $value) {
				
				$attributes_values = $this->prodAttribs[$option][$value];
				
				$attr_value = $attributes_values['options_values_name'];
				$attr_field = tep_draw_hidden_field('id[' . $pId . '][' . $option . ']', $value);
				if($value == OPTIONS_VALUE_TEXT_ID) {
					$attr_value = $prod['attributes_values'][$option];
					$attr_field = tep_draw_hidden_field('id[' . $pId . '][' . TEXT_PREFIX . $option . ']', $attr_value);
				}
				
				$_price_prefix = $attributes_values['price_prefix'];
				$_price = $attributes_values['options_values_price'];
				if(ENABLE_CUSTOMER_GROUPS > 0 && isset($attributes_values['groups_values_price']) && $attributes_values['groups_values_price'] != 0) {
					$_price = $attributes_values['groups_values_price'];
					$_price_prefix = $attributes_values['groups_price_prefix'];
				}
				
				//BOF - Zappo - Option Types v2 - Rearanged Product(s) cart-listing, added Options Column, Upload preview link, and added Prices to Attributes
				// $imageDir = (tep_session_is_registered('customer_id') && $customer_id > 0 ? UPL_DIR : TMP_DIR);
				$imageDir = UPL_DIR;
				$_link_start = '';
				$_link_end = '';
				if(file_exists($imageDir . $attr_value)) {
					$_link_start = '<a href="' . $imageDir . $attr_value . '" target="_blank">';
					$_link_end = tep_image(DIR_WS_ICONS . 'view.gif') . '</a>';
				}
				
				$_pricePre = '';
				$_pricePost = '';
				if($attributes_values['products_options_type'] == OPTIONS_TYPE_QUANTITY && $_price != 0 && (int)$value > 0) {
					$_qty = (int)$prod['attributes_values'][$option];
					$_pricePre .= $currencies->display_price($_price, tep_get_tax_rate($prod['tax_class_id'])) . " x {$_qty} = ";
					$_price *= $_qty;
				}
				
				$Option_Price = ($_price != 0 ? ' - (' . $_price_prefix . $_pricePre . $currencies->display_price($_price, tep_get_tax_rate($prod['tax_class_id'])) . $_pricePost . ')' : '');
				$optsOut .= "<li>- {$attributes_values['products_options_name']}: <em>{$_link_start}{$attr_value} {$_link_end}</em>{$attr_field} {$Option_Price}</li>\n";
				//BOF quick rfq checkout
				$attribs_array[$attributes_values['products_options_name']]= $attr_value;
				//EOF - Zappo - Option Types v2 - Rearanged Product(s) cart-listing, added Options Column, Upload preview link, and added Prices to Attributes
			}
			$optsOut .= "</ul>";
	
		}
		if(!$return_array) {
		return $optsOut;
   	     }else{
		    return $attribs_array;
     	   }
	   			//EOF quick rfq checkout 
	}
	
	// Get attributes listing for receipt display
	public function draw_receipt_summary($oObj, $pidx) {
		global $currencies;
		
		$attribHtml = '';
		
		// dump($oObj);
		// die('Here ' . sizeof($oObj->products[$pidx]['attributes']));
		
		for($j = 0, $k = sizeof($oObj->products[$pidx]['attributes']); $j < $k; $j++) {
			if(tep_not_null($oObj->products[$pidx]['attributes'][$j]['value'])) {
				$attribHtml .= '<li><em>';
				if($oObj->products[$pidx]['attributes'][$j]['type'] == OPTIONS_TYPE_FILE) {
					$attribHtml .= $oObj->products[$pidx]['attributes'][$j]['option'];
					$attribHtml .= ': <a href="' . tep_href_link(UPL_DIR . $oObj->products[$pidx]['attributes'][$j]['value'], '', 'NONSSL') . '" target="_blank">';
					$attribHtml .= $oObj->products[$pidx]['attributes'][$j]['value'] . '</a>';
				} else {
					$attribHtml .= $oObj->products[$pidx]['attributes'][$j]['option'] . ': ' . $oObj->products[$pidx]['attributes'][$j]['value'];
				}
				if( $cg_row = $this->get_group_attribute_pricing($customer_group_id, $products_attribs_array['products_attributes_id']) ) {
					if($cg_row['options_values_price'] != 0) {
						if($oObj->products[$pidx]['attributes'][$j]['type'] == OPTIONS_TYPE_QUANTITY) {
							$_aPrice = $oObj->products[$pidx]['attributes'][$j]['value'] * $cg_row['options_values_price'];
							$_aPrice = $currencies->format($_aPrice, true, $oObj->info['currency'], $oObj->info['currency_value']);
							$attribHtml .= ' (' . $cg_row['price_prefix'] . $oObj->products[$pidx]['attributes'][$j]['value'] . ' x ' . $currencies->format($cg_row['options_values_price'], true, $oObj->info['currency'], $oObj->info['currency_value']) . " = {$_aPrice}" .')';
						} else {
							$attribHtml .= ' (' . $cg_row['price_prefix'] . $oObj->products[$pidx]['qty'] . ' x ' . $currencies->format($cg_row['options_values_price'], true, $oObj->info['currency'], $oObj->info['currency_value']) .')';
						}
					}
				} elseif($oObj->products[$pidx]['attributes'][$j]['price'] > 0) {
					if($oObj->products[$pidx]['attributes'][$j]['type'] == OPTIONS_TYPE_QUANTITY) {
						$_aPrice = $oObj->products[$pidx]['attributes'][$j]['value'] * $oObj->products[$pidx]['attributes'][$j]['price'];
						$_aPrice = $currencies->format($_aPrice, true, $oObj->info['currency'], $oObj->info['currency_value']);
						$attribHtml .= ' (' . $oObj->products[$pidx]['attributes'][$j]['prefix'] . $oObj->products[$pidx]['attributes'][$j]['value'] . ' x ' . $currencies->format($oObj->products[$pidx]['attributes'][$j]['price'], true, $oObj->info['currency'], $oObj->info['currency_value']) . " = {$_aPrice}" .')';
					} else {
						$attribHtml .= ' (' . $oObj->products[$pidx]['attributes'][$j]['prefix'] . $oObj->products[$pidx]['qty'] . ' x ' . $currencies->format($oObj->products[$pidx]['attributes'][$j]['price'], true, $oObj->info['currency'], $oObj->info['currency_value']) .')';
					}
				}
				$attribHtml .= '</em></li>';
			}
		}
		if(tep_not_null($attribHtml)) {
			return '<ul class="noindent">' . $attribHtml . '</ul>';
		}
		return '';
		
	}
	
}


