#Cart Schema
 
This will add the normal schema request made by SEO departments to a OS Commerce Cart.  

There are 4 requirements for proper operation and one optional file 
* schemaOrg.php ( placed in the includes directory)
* schemaSetup.php (placed in the admin)
* schema.php (placed in the admin)
* require schemaOrg.php inside of the shared template below the footer.

Below are the setup instructions

put the schemaSetup.php into the admin and then go to /admin/schemaSetup.php   There is a report that is generated  
when the scripts are ran.  Everything database side will happen. You may need to manually do the following

* place schemaOrg.php in the includes Directory.   Sometimes it is locked and not writable  you can get this file from  
the repo
* If your site offers teired pricing ( price breaks) you may need to modify the products_attributes.php file manually.  
there is one variable that needs to be added. The file can be brougth in from the repo if you have not modified yours  
otherwise, compare and find all usages of $multiPricingArray 
* Lastly,  you may need to go into your shared_template.php and add a line of code to require schemaOrg.php below the  
footer as that is best practice

###What does each file do?

* schemaOrg.php is the magic.  Its setting all of the schema variables and putting them on the front end.
* schema.php is the admin interface so you can add/modify your attributes for schema as needed
* schemaSetup.php is waht it sounds like.  It sets up everything that is required to make it work

